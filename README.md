## Começando

Para executar o projeto, será necessário instalar os seguintes programas:

- [Eclipse: Para desenvolvimento do projeto](https://download.springsource.com/release/STS4/4.9.0.RELEASE/dist/e4.18/spring-tool-suite-4-4.9.0.RELEASE-e4.18.0-win32.win32.x86_64.self-extracting.jar)


### Construção

Para construir o projeto com o Maven, executar os comando abaixo:

```shell
mvn clean install
```

O comando irá baixar todas as dependências do projeto e criar um diretório *target* com os artefatos construídos. Além disso, serão executados os testes unitários, e se algum falhar, o Maven exibirá essa informação no console.



### Configuração

Para conexão com o banco de dados está sendo utilizado o MYSQL, a criação do esquema na base de dados é automática, mas utilização do nome e senha do usuário são configuráveis no arquivo application.properties. Exemplo:

```shell
spring.datasource.username=root
spring.datasource.password=1234
```

O projeto está configurado para execuçõ na porta padrão 8080







