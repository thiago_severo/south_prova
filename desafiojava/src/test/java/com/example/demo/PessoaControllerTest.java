package com.example.demo;

import static org.hamcrest.CoreMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.BDDMockito.given;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.java.app.DesafiojavaApplication;
import com.java.app.model.Pessoa;
import com.java.app.service.PessoaService;

@SpringBootTest(classes = DesafiojavaApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
public class PessoaControllerTest {

    
    @InjectMocks
    private PessoaControllerTest dishController;

    @Mock
    private PessoaService userService;

    @Mock
    PessoaService policyService;

    @Autowired
    WebApplicationContext wac;

    private MockMvc mockMvc;
    
  
    @Before
    public void setup() {

         MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).dispatchOptions(true).build();

    }
    
    @Test
    public void shouldCreateNewPessoaFisica() throws Exception {
        
    	 mockMvc.perform(
                  put("/pessoa")
                  .contentType(MediaType.APPLICATION_JSON)
    	    .content("{   \"pessoa\":{	\"tipo\": \"PF\",\"documento\": \"07339882559\"			}   }"))
    	 .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print())   
                 .andExpect(status().isOk());
         
              
    }
    
    @Test
    public void shouldCreateNewPessoaJuridica() throws Exception {
        
    	 mockMvc.perform(
                  put("/pessoa")
                  .contentType(MediaType.APPLICATION_JSON)
    	    .content("{   \"pessoa\":{	\"tipo\": \"PJ\",\"documento\": \"07339882559001\"			}   }"))
    	 .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print())   
                 .andExpect(status().isOk());
         
              
    }
    
    
    @Test
    public void CreateNewPessoaJuridicaBadRequest() throws Exception {
        
    	 mockMvc.perform(
                  put("/pessoa")
                  .contentType(MediaType.APPLICATION_JSON)
    	    .content("{   \"pessoa\":{	\"tipo\": \"P\",\"documento\": \"398825\"			}   }"))
    	 .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print())   
                 .andExpect(status().isInternalServerError());
         
              
    }
    
}
