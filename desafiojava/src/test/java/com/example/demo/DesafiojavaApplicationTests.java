package com.example.demo;


import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.java.app.model.Pessoa;
import com.java.app.repository.PessoaRepository;
import com.java.app.service.PessoaService;
import com.java.app.service.PessoaServiceImpl;

import org.junit.jupiter.api.Assertions;


@RunWith(MockitoJUnitRunner.class)
public class DesafiojavaApplicationTests {

	@Mock
    private static PessoaRepository pessoaRepository;

    @InjectMocks
    private static PessoaService accountService = new PessoaServiceImpl();

    @Test
    public void testSavePessoa() {

        Pessoa pessoa = new Pessoa();
        pessoa.setScore(4);
        pessoa.setTipo("PF");
        
        Pessoa pessoaSave = pessoa;
        pessoaSave.setId(1);
        
        Mockito.when(pessoaRepository.save(pessoa)).thenReturn(pessoaSave);
        Pessoa retrivedPessoa = accountService.adicionar(pessoa);

        Assert.assertEquals(pessoaSave, retrivedPessoa);

        
        
    }
	
    @Test (expected = javax.persistence.PersistenceException.class)
    public void testSavePessoaErro() {

        Pessoa pessoa = new Pessoa();
        pessoa.setScore(4);
        pessoa.setTipo("PF");
        
        Mockito.when(pessoaRepository.save(pessoa)).thenThrow(new javax.persistence.PersistenceException());
         accountService.adicionar(pessoa);

        
    }
    
}
