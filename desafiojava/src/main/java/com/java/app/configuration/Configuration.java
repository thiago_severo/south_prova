package com.java.app.configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.java.app.model.Limite;
import com.java.app.model.Pessoa;
import com.java.app.model.PessoaFisica;
import com.java.app.model.PessoaJuridica;

import br.com.caelum.stella.validation.CPFValidator;

@Component
public class Configuration {

	@Value("${app.agencia}")
	private String agencia;

	@Value("${app.limiteChequeLv1}")
	private double limiteChequeLv1;

	@Value("${app.limiteChequeLv2}")
	private double limiteChequeLv2;

	@Value("${app.limiteChequeLv3}")
	private double limiteChequeLv3;

	@Value("${app.limiteCartaoLv1}")
	private double limiteCartaoLv1;

	@Value("${app.limiteCartaoLv2}")
	private double limiteCartaoLv2;

	@Value("${app.limiteCartaoLv3}")
	private double limiteCartaoLv3;

	public String getAgencia() {
		return agencia;
	}

	public double getLimiteChequeLv1() {
		return limiteChequeLv1;
	}

	public double getLimiteChequeLv2() {
		return limiteChequeLv2;
	}

	public double getLimiteChequeLv3() {
		return limiteChequeLv3;
	}

	public double getLimiteCard(int score) {

		if (score >= 2 && score <= 5) {
			return limiteCartaoLv1;
		} else if (score >= 6 && score <= 8) {
			return limiteCartaoLv2;
		} else if (score == 9) {
			return limiteCartaoLv3;
		} else {
			return 0;
		}

	}

	public double getLimiteByScore(int score) {

		if (score >= 2 && score <= 5) {
			return limiteChequeLv1;
		} else if (score >= 6 && score <= 8) {
			return limiteChequeLv2;
		} else if (score == 9) {
			return limiteChequeLv3;
		} else {
			return 0;
		}

	}

	public boolean isEnabled(int score) {

		if (score >= 2 && score <= 5) {
			return true;
		} else if (score >= 6 && score <= 8) {
			return true;
		} else if (score == 9) {
			return true;
		} else {
			return false;
		}

	}

	public Pessoa getPessoa(String documento, int randomScore, String tipoPessoa) {

		Pessoa pessoa = null;

		if (tipoPessoa.equals("PF") && valida(documento) ) {
			pessoa = new PessoaFisica(documento, randomScore, "PF");
		}
		else if (tipoPessoa.equals("PJ") && isValidCNPJ(documento)) {
			pessoa = new PessoaJuridica(documento, randomScore, "PJ");
		}else {
			throw new RuntimeException("parametos inválidos");
		}

		return pessoa;

	}
	
	public static boolean valida(String cpf) { 
	    CPFValidator cpfValidator = new CPFValidator(); 
	    try{ cpfValidator.assertValid(cpf); 
	    return true; 
	    }catch(Exception e){ 
	        e.printStackTrace(); 
	        return false; 
	        } 
	    }
	
	 private static final int[] pesoCNPJ = {6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
	
	 private static int calcularDigito(String str, int[] peso) {
	        int soma = 0;
	        for (int indice=str.length()-1, digito; indice >= 0; indice-- ) {
	            digito = Integer.parseInt(str.substring(indice,indice+1));
	            soma += digito*peso[peso.length-str.length()+indice];
	        }
	        soma = 11 - soma % 11;
	        return soma > 9 ? 0 : soma;
	    }
	 
	 private static boolean isValidCNPJ(String cnpj) {
	        cnpj = cnpj.trim().replace(".", "").replace("-", "");
	        if ((cnpj==null)||(cnpj.length()!=14)) return false;

	        Integer digito1 = calcularDigito(cnpj.substring(0,12), pesoCNPJ);
	        Integer digito2 = calcularDigito(cnpj.substring(0,12) + digito1, pesoCNPJ);
	        return cnpj.equals(cnpj.substring(0,12) + digito1.toString() + digito2.toString());
	    }

}
