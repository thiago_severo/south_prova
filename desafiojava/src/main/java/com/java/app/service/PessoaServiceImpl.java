package com.java.app.service;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.app.model.Pessoa;
import com.java.app.model.PessoaFisica;
import com.java.app.repository.PessoaFisicaRepository;
import com.java.app.repository.PessoaRepository;

@Service
public class PessoaServiceImpl implements PessoaService{

	@PersistenceContext
    private EntityManager em;
	
	@Autowired
	PessoaRepository pessoaRepository;

	@Autowired
	PessoaFisicaRepository pessoaFisicaRepository;

	
	@Override
	public Pessoa adicionar(Pessoa pessoa) {
		return pessoaRepository.save(pessoa);
	}

	@Override
	public Pessoa getById(long id) {
		return pessoaFisicaRepository.getPessoaId(id);
	}
	
	

}
