package com.java.app.service;

import java.util.Optional;

import com.java.app.model.Pessoa;
import com.java.app.model.PessoaFisica;

public interface PessoaService {
	 
	public abstract Pessoa adicionar(Pessoa pessoa);
	public abstract Pessoa getById(long id);
	   
}
