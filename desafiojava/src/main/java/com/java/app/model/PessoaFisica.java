package com.java.app.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.Size;

@Entity
@DiscriminatorValue(value = "PF")
public class PessoaFisica extends Pessoa {

	@Column(length=11)
	@Size(min = 11, max = 11)
    private String documentoPf;


	public PessoaFisica() {
		super();
	}

	public PessoaFisica(@Size(min = 11, max = 11) String documentoPf, int score, String tipo) {
		super(score, tipo);
		this.documentoPf = documentoPf;
	}

	public String getDocumentoPf() {
		return documentoPf;
	}

	public void setDocumentoPf(String documentoPf) {
		this.documentoPf = documentoPf;
	}

    
	
}
