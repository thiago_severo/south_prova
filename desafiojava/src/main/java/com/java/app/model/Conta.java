package com.java.app.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.java.app.DesafiojavaApplication;
import com.java.app.configuration.Configuration;

@Entity
@Table(name = "conta")
public class Conta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String agencia;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "pessoa_id", nullable = false)
	private Pessoa pessoa;

	@Column(length = 1, updatable = false)
	private String tipo;

	@Size(min = 6, max = 6)
	private String numero;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "limite_id", referencedColumnName = "id")
	private Limite limite;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "cartao_id", referencedColumnName = "id")
	private Cartao cartao;
	
	
	
	public Conta() {
		super();
	}

	public Conta(Pessoa pessoa, @Size(min = 6, max = 6) String numero) {

		super();
		this.pessoa = pessoa;
		this.numero = numero;

		if (pessoa.getTipo() == "PF") {
			tipo = "C";
		} else if (pessoa.getTipo() == "PJ") {
			tipo = "E";
		}

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
/*
	public void setCheque(LimiteChequeEspecial cheque) {
		
		if(pessoa.getScore()<=1) {
			cheque.setHabilitado(false);
		}else if(pessoa.getScore()>=2 && pessoa.getScore()<=5) {
			cheque.setHabilitado(true);
			cheque.setLimite(confBean.getLimiteChequeLv1());
		}else if(pessoa.getScore()>=6 && pessoa.getScore()<=8) {
			cheque.setHabilitado(true);
			cheque.setLimite(confBean.getLimiteChequeLv2());
		}else if(pessoa.getScore()==9) {
			cheque.setHabilitado(true);
			cheque.setLimite(confBean.getLimiteChequeLv3());
		}
		
		this.LimiteChequeEspecial = cheque;
	}*/

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	
	
	
    public Limite getLimiteChequeEspecial() {
		return limite;
	}

	public void setLimiteChequeEspecial(Limite limite) {
		this.limite = limite;
	}

	public Limite getLimite() {
		return limite;
	}

	public void setLimite(Limite limite) {
		this.limite = limite;
	}

	public Cartao getCartao() {
		return cartao;
	}

	public void setCartao(Cartao cartao) {
		this.cartao = cartao;
	}
	
	
	

}
