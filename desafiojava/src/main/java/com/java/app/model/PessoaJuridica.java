package com.java.app.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.Size;

@Entity
@DiscriminatorValue(value = "PJ")
public class PessoaJuridica extends Pessoa{


	@Column(length=14)
	@Size(min = 14, max = 14)
	private String documentoPj;
	
	
	
	public PessoaJuridica() {
		super();
	}

	public PessoaJuridica(@Size(min = 14, max = 14) String documentoPj, int score, String tipo) {
		super(score, tipo);
		this.documentoPj = documentoPj;
		// TODO Auto-generated constructor stub
	}
	
	public String getDocumentoPj() {
		return documentoPj;
	}

	public void setDocumentoPj(String documentoPj) {
		this.documentoPj = documentoPj;
	}


}
