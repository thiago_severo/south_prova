package com.java.app.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.java.app.model.Pessoa;

public interface PessoaRepository extends CrudRepository<Pessoa, Long>{

	@Query(value="SELECT * FROM pessoa where id = ?1 LIMIT 1" ,  nativeQuery = true)
    public Pessoa getPessoaById( Long id);
    
}
