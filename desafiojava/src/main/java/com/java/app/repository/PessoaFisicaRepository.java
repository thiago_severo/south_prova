package com.java.app.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.java.app.model.Pessoa;
import com.java.app.model.PessoaFisica;

public interface PessoaFisicaRepository extends CrudRepository<PessoaFisica, Long>{

	@Query(value="SELECT * FROM pessoa where id = ?1 LIMIT 1" ,  nativeQuery = true)
    public PessoaFisica getPessoaById( Long id);
    
	@Query(value="SELECT * FROM pessoa where id = ?1 LIMIT 1" ,  nativeQuery = true)
    public PessoaFisica getPessoaId( Long id);
	
}
