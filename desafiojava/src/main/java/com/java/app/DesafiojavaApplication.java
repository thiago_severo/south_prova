package com.java.app;

import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import com.java.app.configuration.Configuration;

@Component
@SpringBootApplication
public class DesafiojavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafiojavaApplication.class, args);
			}

}
