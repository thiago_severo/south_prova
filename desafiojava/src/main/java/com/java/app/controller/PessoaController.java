package com.java.app.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.java.app.configuration.Configuration;
import com.java.app.model.Cartao;
import com.java.app.model.Conta;
import com.java.app.model.Limite;
import com.java.app.model.Pessoa;
import com.java.app.model.PessoaFisica;
import com.java.app.model.PessoaJuridica;
import com.java.app.repository.PessoaRepository;
import com.java.app.service.PessoaService;

@RequestMapping(path = "/pessoa")
@RestController
public class PessoaController {

	@Autowired
	private Configuration confBean;

	@Autowired
	PessoaService pessoaService;

	@PutMapping(consumes = "application/json", produces = "application/json")
	public Map<String, String> insertPessoa(@RequestBody String request,  HttpServletRequest requestServ, HttpServletResponse responseServ) {

		JSONObject jsonObject = new JSONObject(request);
		Map<String, String> response = new HashMap<>();
		Pessoa pessoa = null;
		Conta conta = null;
		Random random = new Random();
		int numeroConta = random.nextInt(999999);
		int randomScore = random.nextInt(10);

		try {
			
			pessoa = confBean.getPessoa(jsonObject.getJSONObject("pessoa").getString("documento"), randomScore, jsonObject.getJSONObject("pessoa").getString("tipo"));

			conta = new Conta(pessoa, numeroConta + "");
			conta.setAgencia(confBean.getAgencia());

			Limite limite = new Limite();
			limite.setHabilitado(confBean.isEnabled(pessoa.getScore()));
			limite.setLimite(confBean.getLimiteByScore(pessoa.getScore()));
			conta.setLimiteChequeEspecial(limite);
			
			Cartao cartao = new Cartao();
			cartao.setLimite(confBean.getLimiteCard(pessoa.getScore()));
			conta.setCartao(cartao);
			
			Set<Conta> contas = new HashSet<>();
			contas.add(conta);
			pessoa.setConta(contas);
			
			pessoaService.adicionar(pessoa);
			
			response.put("status", "OK");
			response.put("id_pessoa", pessoa.getId()+"");
			
		} catch ( Exception jexception) {
			jexception.printStackTrace();
			response.put("status", "ERRO");
			response.put("detail", jexception.getMessage());
			 responseServ.setStatus( HttpServletResponse.SC_INTERNAL_SERVER_ERROR  );	
		} 
		
		
		return response;

	}
	
	
	@GetMapping("{id}")
	public Map<String, String> getDetailPessoa(@PathVariable Long id){
		
		Map<String, String> response = new HashMap<>();
		Pessoa pessoa = pessoaService.getById(id);
		
		response.put("tipo pessoa", pessoa.getTipo());		
		response.put("score", pessoa.getScore()+"");
		
		Conta conta = pessoa.getConta().iterator().next();
		response.put("conta agencia", conta.getAgencia());
		response.put("conta tipo", conta.getTipo());
		response.put("conta numero", conta.getNumero());
		
		response.put("limite cheque habilitado", conta.getLimite().getHabilitado().toString());
		response.put("limite cheque valor ", conta.getLimite().getLimite()+" ");
		
		if(conta.getCartao()!=null) {
			response.put("limite cartao valor ", conta.getCartao().getLimite()+" ");	
		}
	
		return response;
		
	}

}
